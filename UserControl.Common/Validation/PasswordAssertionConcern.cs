﻿using System.Text;
using UserControl.Common.Resources;

namespace UserControl.Common.Validation
{
    public class PasswordAssertionConcern
    {
        public static void AssertIsValid(string password)
        {
            AssertionConcern.AssertArgumentNotNull(password, Errors.InvalidCredentials);
        }

        public static string Encrypt(string senha)
        {
            senha += "|2e32a80c-25e9-e511-8920-ac7ba1c9b2a5";
            var md5 = System.Security.Cryptography.MD5.Create();
            var data = md5.ComputeHash(Encoding.Default.GetBytes(senha));
            var sbString = new StringBuilder();
            for (var i = 0; i < data.Length; i++) sbString.Append(data[i].ToString("x2"));
            return sbString.ToString();
        }
    }
}