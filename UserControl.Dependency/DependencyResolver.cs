﻿using Microsoft.Practices.Unity;
using UserControl.Domain.Interfaces.Repository;
using UserControl.Domain.Interfaces.Services;
using UserControl.Domain.Services;
using UserControl.Infra.Context;
using UserControl.Infra.Repository;

namespace UserControl.Dependency
{
    public static class DependencyResolver
    {
        public static void Resolve(UnityContainer container)
        {
            container.RegisterType<UserControlContext, UserControlContext>(new HierarchicalLifetimeManager());

            container.RegisterType<IUserRepository, UserRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IUserService, UserService>(new HierarchicalLifetimeManager());
        }
    }
}