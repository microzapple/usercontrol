﻿using System.Data.Entity;
using UserControl.Domain.Models;
using UserControl.Infra.Map;

namespace UserControl.Infra.Context
{
    public class UserControlContext : DbContext
    {
        public UserControlContext() : base("DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            base.OnModelCreating(modelBuilder);
        }
    }
}