﻿using System;
using System.Collections.Generic;
using System.Linq;
using UserControl.Domain.Interfaces.Repository;
using UserControl.Domain.Models;
using UserControl.Infra.Context;

namespace UserControl.Infra.Repository
{
    public class UserRepository : IUserRepository
    {
        private UserControlContext Db;

        public UserRepository(UserControlContext db)
        {
            Db = db;
        }

        public void Create(User user)
        {
            Db.Users.Add(user);
            Db.SaveChanges();
        }

        public void Update(User user)
        {
            var b = Db.Entry<User>(user).State == System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();
        }

        public User GetById(Guid id)
        {
            return Db.Users.Find(id);
        }

        public User GetByEmail(string email)
        {
            //First dá uma execeção caso não exista
            //FirstOrDefault return null se não existir

            return Db.Users.FirstOrDefault(x => string.Equals(x.Email, email));
        }

        public ICollection<User> GetByRange(int skip, int take)
        {
            return Db.Users.OrderBy(x => x.FirstName).Skip(skip).Take(take).ToList();
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}