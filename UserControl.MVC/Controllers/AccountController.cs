﻿using System.Web;
using System.Web.Mvc;
using UserControl.Domain.Interfaces.Services;
using UserControl.MVC.ViewModels;

namespace UserControl.MVC.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IUserService _user;

        public AccountController(IUserService user)
        {
            _user = user;
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(_user.GetByRange(0, 10));
        }

        public ActionResult Teste()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {

            var isA = User.Identity.IsAuthenticated;
            return new ChallengeResult("Google", Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginVm model)
        {
            if (ModelState.IsValid)
            {
                _user.Authenticate(model.Email, model.Password);
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("DefaultErrorMessage", "Not Authenticate");
            return View(model);
        }

        public ActionResult Logout()
        {
            var context = Request.GetOwinContext();
            var authManager = context.Authentication;
            authManager.SignOut();
            var isA = User.Identity.IsAuthenticated;
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {

            var context = Request.GetOwinContext().Authentication.User.Identity.IsAuthenticated;

            if (string.IsNullOrEmpty(returnUrl)) return RedirectToAction("Index");
            return new RedirectResult(returnUrl);
        }

        protected override void Dispose(bool disposing)
        {
            _user.Dispose();
        }
    }
}