﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(UserControl.MVC.Startup))]
namespace UserControl.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}