﻿using System.Web.Mvc;
using System.Web.Routing;

namespace UserControl.MVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //TODO Obrigatório a chamada para a injeção de dependencias
            Bootstrapper.Initialise();
        }
    }
}
