using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using UserControl.Domain.Interfaces.Repository;
using UserControl.Domain.Interfaces.Services;
using UserControl.Domain.Services;
using UserControl.Infra.Context;
using UserControl.Infra.Repository;

namespace UserControl.MVC
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();
            
            container.RegisterType<UserControlContext, UserControlContext>(new HierarchicalLifetimeManager());

            container.RegisterType<IUserRepository, UserRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IUserService, UserService>(new HierarchicalLifetimeManager());
     
            return container;
        }
    }
}