﻿using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;

namespace UserControl.MVC.ViewModels
{
    public class ChallengeResult : HttpUnauthorizedResult
    {
        public ChallengeResult(string provider, string redirectUri)
        {
            LoginProvider = provider;
            RedirectUri = redirectUri;
        }

        public string LoginProvider { get; set; }
        public string RedirectUri { get; set; }

        public void LogOut(ControllerContext context)
        {
           //context.HttpContext.GetOwinContext().Authentication.SignOut();
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
            var owin = context.HttpContext.GetOwinContext().Authentication;
            owin.Challenge(properties, LoginProvider);
        }
    }
}