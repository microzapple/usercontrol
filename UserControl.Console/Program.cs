﻿using System;
using UserControl.Domain.Interfaces.Repository;
using UserControl.Domain.Models;
using UserControl.Infra.Repository;

namespace UserControl.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            const string email = "oziel.guimaraes@gmail.com";
            const string firstName = "Oziel";
            const string lastName = "Guimarães";
            const string password = "1234567890";
            const string confirmPassword = password;

            var user = new User(firstName, lastName, email) { Active = true };
            user.SetPassword(password, confirmPassword);
            user.Validate();
            //using (IUserRepository repository = new UserRepository())
            //{
            //    repository.Create(user);
            //}
            //using (IUserRepository repository = new UserRepository())
            //{
            //    var usr = repository.GetByEmail(email);
            //    System.Console.WriteLine(usr.FirstName);
            //}
            //using (IUserRepository up = new UserRepository())
            //{
            //    var usr = up.GetByEmail(email);
            //    usr.ChangeEmail("novo@gmail.com");
            //    up.Update(usr);
            //    var newu = up.GetByEmail(usr.Email);
            //    System.Console.WriteLine(newu.Email);
            //}

            System.Console.ReadKey();
        }
    }
}