﻿using System;
using UserControl.Common.Resources;
using UserControl.Common.Validation;

namespace UserControl.Domain.Models
{
    public class User
    {
        #region Properties
        public Guid UserId { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
        public bool Active { get; set; }
        #endregion

        #region Methods
        protected User() { }
        public User(string firstName, string lastName, string email)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }

        public void SetPassword(string password, string confirmPassword)
        {
            AssertionConcern.AssertArgumentNotNull(password, Errors.Required);
            AssertionConcern.AssertArgumentNotNull(confirmPassword, Errors.RequiredConfirmPassword);
            AssertionConcern.AssertArgumentLength(password, 5, 10, Errors.InvalidLimitPassword);
            AssertionConcern.AssertArgumentEquals(password, confirmPassword, Errors.PasswordDoesMatch);

            Password = PasswordAssertionConcern.Encrypt(password);
        }
        public string ResetPassword()
        {
            var password = Guid.NewGuid().ToString().Substring(0, 8);
            Password = PasswordAssertionConcern.Encrypt(password);

            return password;
        }
        public void ChangeEmail(string email)
        {
            Email = email;
        }
        public void Validate()
        {
            PasswordAssertionConcern.AssertIsValid(Password);
            if (Email != null) EmailAssertionConcern.AssertIsValid(Email);
        }
        #endregion
    }
}