﻿using System;
using System.Collections.Generic;
using System.Linq;
using UserControl.Common.Resources;
using UserControl.Common.Validation;
using UserControl.Domain.Interfaces.Repository;
using UserControl.Domain.Interfaces.Services;
using UserControl.Domain.Models;

namespace UserControl.Domain.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _repository;

        public UserService(IUserRepository repository)
        {
            _repository = repository;
        }

        public User Authenticate(string email, string password)
        {
            var user = GetByEmail(email);

            if (user.Password != PasswordAssertionConcern.Encrypt(password))
                throw new Exception(Errors.InvalidCredentials);

            return user;
        }

        public void ChangeInformation(string email, string newEmail)
        {
            var user = GetByEmail(email);

            user.ChangeEmail(newEmail);
            user.Validate();

            _repository.Update(user);
        }

        public void ChangePassword(string email, string password, string newPassword, string confirmNewPassword)
        {
            var user = Authenticate(email, password);

            user.SetPassword(newPassword, confirmNewPassword);
            user.Validate();

            _repository.Update(user);
        }

        public void Register(string firstName, string lastName, string email, string password, string confirmPassword)
        {
            var hasUser = _repository.GetByEmail(email);
            if (hasUser != null)
                throw new Exception(Errors.DuplicateEmail);

            var user = new User(firstName, lastName, email);
            user.SetPassword(password, confirmPassword);
            user.Validate();

            _repository.Create(user);
        }

        public User GetById(Guid id)
        {
            var user = _repository.GetById(id);
            if (user == null)
                throw new Exception(Errors.UserNotFound);

            return user;
        }

        public User GetByEmail(string email)
        {
            var user = _repository.GetByEmail(email);
            if (user == null)
                throw new Exception(Errors.UserNotFound);

            return user;
        }

        public string ResetPassword(string email)
        {
            var user = GetByEmail(email);
            var password = user.ResetPassword();
            user.Validate();

            _repository.Update(user);
            return password;
        }

        public List<User> GetByRange(int skip, int take)
        {
            return _repository.GetByRange(skip, take).ToList();
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}
