﻿using System;
using System.Collections.Generic;
using UserControl.Domain.Models;

namespace UserControl.Domain.Interfaces.Repository
{
    public interface IUserRepository : IDisposable
    {
        void Create(User user);
        void Update(User user);
        User GetById(Guid id);
        User GetByEmail(string email);
        ICollection<User> GetByRange(int skip, int take);
    }
}