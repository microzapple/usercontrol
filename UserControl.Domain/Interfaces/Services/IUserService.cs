﻿using System;
using System.Collections.Generic;
using UserControl.Domain.Models;

namespace UserControl.Domain.Interfaces.Services
{
    public interface IUserService : IDisposable
    {
       // void Register(User model);
        User GetById(Guid id);
        User GetByEmail(string email);
        //User GetByUserName(string userName);
        User Authenticate(string userName, string password);
        //void AddUserCourse(UserCurso userCourse, ICollection<UserCurso> usersCourse);
       void Register(string firstName, string lastName, string email, string password, string confirmPassword);
        void ChangeInformation(string email, string name);
        void ChangePassword(string email, string password, string newPassword, string confirmNewPassword);
        string ResetPassword(string email);
        List<User> GetByRange(int skip, int take);
    }
}